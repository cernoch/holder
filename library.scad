rib_count = 5; // number of ribs
rib_space = (48/5)*0.97; // rib-to-rib distance (including the rib width)
rib_width = rib_space - 3/0.97; // width of one rib
rib_angle = 10; // angle of the ribs wrt. the normal
rib_length = 30; // length of the longest rib (in the middle)

insert_height = 10; // 80; // how high will the insert be

insert_hollow = 3; // difference between thinnest and thickest points
insert_protrusion = 8; // amount of material at thinnest point
insert_width = (rib_count-1) * rib_space + rib_width;
insert_base = insert_width / cos(rib_angle);
insert_radius = (insert_base*insert_base + 4*insert_hollow*insert_hollow) / (8*insert_hollow);

// offset between inserts below the plate
insert_spacing = [-74.75, +74.75] / 2;
// Skoda Fabia 2007: insert_spacing = [-74.75, +74.75] / 2;
// Skoda Fabia 2007: insert_spacing = [-206, -56.5, +56.5, +206] / 2;

insert_hole_tolerance = 0.5;

magnet_spacing = 70; // distance between magnet centers
magnet_radius = 27/2;
magnet_thick = 7;
magnet_space_above = 0; // amount of space between the magnet and the phone

magnet_screw_diameter = 5; // magnet is held down by M5 screw
magnet_nut_height = 4.5; // height of a M5 nut
magnet_nut_diameter = 8.3; // diameter of a M5 nut
magnet_nut_tolerance = 0; // space around the magnet

phone_height = 4; // height of the corner bracing
phone_wall_thickness = 3; // thickness of corner bracing

plate_width = 163 + 2*phone_wall_thickness;
plate_height = 82 + phone_wall_thickness;
plate_thick = 20;
plate_corner = 10; // Radius of the plate corner
// Caterpillar S52:
//      plate_width = 159 + 2*phone_wall_thickness;
//      plate_height = 82 + phone_wall_thickness;

// protrusion for the camera 
camera_width = 35; 
camera_height = 20;
camera_thickness = -1; // -1 disables the camera
camera_radius = 6;

// position of the camera
camera_offset_width = 6;
// align left: set to 0
// right: set to (plate_width - camera_width)
// center: set to (plate_width - camera_width)/2
camera_offset_height = plate_height - camera_height - 6;
// align bottom: set to 0
// top: set to (plate_height - camera_height)
// center: set to (plate_height - camera_height)/2

assert(2 * camera_radius < camera_width);
assert(2 * camera_radius < camera_height);


include <slicing.scad>

module insert(negative_mode = false)
{
    difference()
    {
        // Main body
        tolerance_vector = !negative_mode ? [ 0, 0, 0 ]
            : [ insert_hole_tolerance, 0, insert_hole_tolerance ];

        translate(- tolerance_vector / 2)
            rotate([ 0, 0, -rib_angle ])
                cube([ insert_width, insert_radius, insert_height ] + tolerance_vector);

        // Round internal shape
        if (!negative_mode)
        {
            offset = (insert_hollow + insert_protrusion) / tan(90-rib_angle);
            translate([ insert_base/2 + offset, insert_radius + insert_protrusion, -1 ])
                cylinder(r = insert_radius, h = insert_height + 2, $fa = 1);
        }

        // Trim and create the base plate faset
        if (rib_angle > 0)
            translate([ -1, -insert_radius, -1 ])
                cube([ insert_width + 2, insert_radius, insert_height + 2 ]);
    }

    // Ribs
    if (!negative_mode)
        for (i = [ 1:rib_count ])
            rotate([ 0, 0, -rib_angle ])
                translate([ 0, insert_protrusion, 0 ]
                        + [ 1, sin(rib_angle), 0 ] * (i-1) * rib_space)
                {
                    // Main part of the rib
                    cube([ rib_width, rib_length, insert_height ]);
                    // Make the tip of the rib round
                    translate([ rib_width/2, rib_length, 0 ])
                        cylinder(r = rib_width/2, h = insert_height, $fn = 16);
                }
}

module plate()
{
    module minus_circle()
    {
        difference()
        {
            union() children();
            circle(r = plate_corner, $fn = 64);
        }
    }

    module magnet_and_screw()
    {
        // hole for the magnet
        translate([0, 0, plate_thick - magnet_thick - magnet_space_above])
            cylinder(r = magnet_radius, h = magnet_thick + magnet_space_above + 1);
        
        // hole for the screw
        translate([0, 0, -1])
        {
            // hole for the screw
            cylinder(d = magnet_screw_diameter + 0.5, // some tolerance
                     h = plate_thick + 2, $fn = 16);

            // hole for the nut
            cylinder(d = magnet_nut_diameter + magnet_nut_tolerance + 1,
                     h = magnet_nut_height + magnet_nut_tolerance + 1,
                     $fn = 6);
        }
    }

    ////////////////
    // Main plate //
    ////////////////

    chamfer = 2;
    intersection()
    {
        // bounding box trims excess minkowski
        cube([plate_width, plate_height, plate_thick]);

        difference()
        {    
            translate(chamfer*[1,1,0]) // bottom is flat for 3D printing
                minkowski()
                {
                    sphere(r = chamfer, $fn = 20); // shape of round edges

                    difference()
                    {
                        // this is actually the main plate
                        cube([plate_width, plate_height, plate_thick] - chamfer*[2,2,1]);

                        // round corners
                        translate([0,0,-1]) linear_extrude(height = plate_thick+2)
                        {
                            x = [plate_corner,plate_corner] + [1,1];
                            translate([plate_corner,plate_corner]) minus_circle() translate(-x) square(x);
                            translate([plate_width-plate_corner-2*chamfer, plate_corner]) minus_circle() translate([0,-plate_corner-1]) square(x);
                            translate([plate_corner, plate_height-plate_corner-2*chamfer]) minus_circle() translate([-plate_corner-1,0]) square(x);
                            translate([plate_width-plate_corner-2*chamfer, plate_height-plate_corner-2*chamfer]) minus_circle() square(x);
                        }
                    }
                }

            // hole for the magnet
            translate([plate_width/2, plate_height/2, 0]) magnet_and_screw();

            // holes for the inserts
            translate([plate_width/2, plate_height/2, 0])
            {
                for (offset = insert_spacing)
                {
                    translate([offset, 0, insert_protrusion])
                    {
                        translate(- [ insert_height,
                                      insert_width + insert_protrusion,
                                      0] / 2)
                            mirror([0, 0, 1])
                                rotate([90, 0, 90])
                                    insert(true);
                    }
                }
            }

            // protrusion for the camera
            translate([camera_offset_width, camera_offset_height, plate_thick - camera_thickness ])
            {
                translate(camera_radius * [1,1,0] + [0,0,1])
                    minkowski()
                    {
                        cube([ camera_width - 2*camera_radius,
                               camera_height - 2*camera_radius,
                               max(camera_thickness - 1, 1) ]);
                        scale([1, 1, 1/camera_radius])
                            sphere(r = camera_radius, $fn = 40);
                    }
            } 
        }
    }

    ///////////////////////////
    // Corners at the bottom //
    ///////////////////////////

    module corner()
    {
        translate([plate_corner, plate_corner, 0])
            angled(180, 270, 1000, 1000)
                cylinder_hollow(r = plate_corner + phone_wall_thickness,
                                h = phone_height + plate_thick,
                                wall = 2*phone_wall_thickness, $fn = 60);

        translate([ plate_corner, 0, 0 ])
            cylinder(r = phone_wall_thickness, h = phone_height + plate_thick, $fn = 16);
        translate([ 0, plate_corner, 0 ])
            cylinder(r = phone_wall_thickness, h = phone_height + plate_thick, $fn = 16);
    }

    corner();
    translate([plate_width, 0, 0])
        rotate([0,0,90]) corner();
}

module plate_slicing_bounding_box(i)
{
    penetration = 15;
    offsets = [ plate_width/2, plate_width + 2*phone_wall_thickness ];

    union() {
        // slice the main part of the plate
        translate([0, phone_wall_thickness, 0]) rotate([-90,0,0])
            translate([- phone_wall_thickness, - plate_thick - phone_height, - phone_wall_thickness])
                slicing(offsets, i, plate_thick + phone_height, plate_height,
                        target_base_width = 14, hole_width = 8, penetration = penetration,
                        lip_thickness = 2 + phone_height, tolerance = 0.1);
        
        // don't forget the overhanging plate border
        translate([ (i==0) ? 0 : offsets[i-1] - phone_wall_thickness,
                    -phone_wall_thickness, 0])
            cube([ plate_width/2 + 2*phone_wall_thickness,
                   phone_wall_thickness,
                   plate_thick + phone_height ]);
    }
}

module cylinder_hollow(r,h,wall, $fn=$fn, $fa=$fa)
{
    difference()
    {
        cylinder(r=r, h=h, $fn=$fn, $fa=$fa);
        translate([0,0,-1])
            cylinder(r=(r-wall), h=(h+2), $fn=$fn, $fa=$fa);
    }
}

module angled(a,b, radius, height, $fn=$fn, $fa=$fa)
{
    intersection()
    {
        translate([0, 0, -1])
        rotate([0, 0, a])
            angle_wedge(b-a, radius, height+2, $fn=$fn, $fa=$fa);
        
        children();
    }
}

module angle_wedge(angle, radius, height, $fn=$fn, $fa=$fa)
{
    if (angle > 0)
    {
        if (angle >= 360)
        {
            cylinder(r=radius, h=(height+2), $fn=$fn, $fa=$fa);

        }
        else intersection()
        {

            translate([ 0, 0, -1 ])
                cylinder(r=radius, h=(height+2), $fn=$fn, $fa=$fa);

            if (angle < 180) intersection()
            {
                translate([-radius, 0, 0])
                    cube([2*radius, 2*radius, height]);

                rotate([0, 0, angle])
                    translate([-radius, -2*radius, 0])
                        cube([2*radius, 2*radius, height]);
            }
            else union()
            {
                translate([-radius, 0, 0])
                    cube([2*radius, 2*radius, height]);
                rotate([0, 0, 180])
                    angle_wedge(angle - 180, radius, height);
            }
        }
    }
}

