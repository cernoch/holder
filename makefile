STL=\
insert.stl\
plate0.stl\
plate1.stl\
plate2.stl\
ranger-part1.stl\
ranger-part2.stl\

LIB=\
library.scad\
slicing.scad\
teardrop.scad\
ranger-lib.scad\

ifeq ($(OS),Windows_NT)
OPENSCAD="C:\Program Files\OpenSCAD\openscad.exe"
else
OPENSCAD=openscad
endif

all: $(STL)

%.stl: %.scad $(LIB)
	$(OPENSCAD) -q -o $@ $<

clean:
	rm -f $(STL)