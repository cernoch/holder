include <library.scad>

// disable the mounting brackets
insert_spacing=[];

plate_tilt = 15; // angle of tilt

connector_thickness = 12;
connector_width = 55;
connector_height = 10;

module round_cube(dims, radius)
{
        linear_extrude(dims.z)
            intersection()
            {
                translate(radius * [1,1])
                    minkowski()
                    {
                        square([dims.x, dims.y] - radius * [2,1]);
                        circle(r=radius, $fn = 16);
                    }
                square([dims.x, dims.y]);
            }
}

module plate_with_connector()
{
    difference()
    {
        translate([0, 0, connector_height])
            rotate([-plate_tilt, 0, 0])
            {
                // the plate itself
                translate([-plate_width/2, 0, 0])
                    rotate([90, 0, 0]) plate();

                connector_adjusted_height = connector_height
                    + sin(plate_tilt) * connector_thickness;

                // connector
                translate([-connector_width/2,
                            -connector_thickness,
                            -connector_adjusted_height])
                    difference()
                    {
                        round_cube([connector_width,
                                    connector_thickness,
                                    connector_adjusted_height], 3);
                        rotate([plate_tilt, 0, 0])
                            translate(-[1, 0, connector_adjusted_height])
                                cube([connector_width + 2,
                                        connector_thickness + 1000,
                                        connector_adjusted_height]);
                    }
            }
        connecting_screws();
    }
}

module ranger_tray()
{
    difference()
    {
        // hand-tuned to align with xy plane
        translate([0, 30, -20])
            scale([41/40, 1, 1])
                import("ranger-tray.stl", convexity=24);

        connecting_screws();
    }
}

module connecting_screws()
{
    translate(-[0, connector_thickness * sin(plate_tilt)
                    + ranger_screw_diameter, 14])
        for (i = [-1, +1])
            translate(i * [connector_width/2 - 10, 0, 0])
                // This is here for compatibility, can be
                // removed once printed from scaratch.
                translate([0,0,ranger_square_nut_height])
                    ranger_square_negative();
}

module prism(
    id, id1, id2, // inner diameters
    h, // height of the prism
    n) // number of sides
{
    assert((id1 == undef) == (id2 == undef));
    assert((id1 == undef) != (id == undef));
    assert(h != undef);
    assert(n >= 3);

    angle = 180 / n;
    d1 = (id1 == undef ? id : id1) / cos(angle);
    d2 = (id2 == undef ? id : id2) / cos(angle);
    rotate([0,0,angle])
        cylinder(d1=d1,d2=d2,h=h,$fn=n);
}


ranger_screw_diameter = 5;
ranger_screw_head_diameter = 9;
ranger_screw_head_height = 3;
ranger_screw_length = 30; // screw length + some slack space
ranger_square_nut_offset = 30; // distance from screw head to the nut's top
ranger_square_nut_side = 8.5;
ranger_square_nut_height = 4;

module ranger_square_negative()
{
    // screw shaft
    prism(id = ranger_screw_diameter + 0.5, // some tolerance
          h = ranger_screw_length, n = 8);

    // screw head
    prism(id1 = ranger_screw_head_diameter + 0.5, // some tolerance
          id2 = ranger_screw_diameter + 0.5,
          h = ranger_screw_head_height, n = 8);

    // screw insertion hole
    translate([0, 0, 0.01-1000])
        prism(id = ranger_screw_head_diameter,
              h = 1000,
              n = 8);

    // screw nut + its insertion hole
    translate([-ranger_square_nut_side/2,
               -ranger_square_nut_side/2,
                ranger_square_nut_offset
               -ranger_square_nut_height])
        cube([ranger_square_nut_side,
              1000, // hole no longer than 1m
              ranger_square_nut_height]);
}
