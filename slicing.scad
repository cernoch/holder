use <teardrop.scad>

joinery = true;

module slicing_one_bounding_box(
    offsets, i, y, z,
    target_base_width,
    hole_width,
    penetration,
    lip_thickness,
    positive_mode,
    bordered_offset,
    tolerance)
{
  if (i == 0)
  {
    translate([offsets[i] - penetration, 0, z])
      rotate([-90, 90, 0])
        if (joinery)
          teardrop_cube([ z, offsets[i] - penetration, y ],
                        target_base_width = target_base_width,
                        hole_width = hole_width,
                        penetration = penetration,
                        lip_thickness = lip_thickness,
                        positive_mode = positive_mode,
                        bordered_offset = bordered_offset,
                        tolerance = tolerance);
        else
          cube([ z, offsets[i] - penetration, y ]);

  } else if ((i+1) == len(offsets)) {
    translate([offsets[i-1] - penetration, 0, 0])
      cube([offsets[i] - offsets[i-1] + penetration, y, z]);

  } else {
    translate([offsets[i] - penetration, 0, z])
      rotate([-90, 90, 0])
        if (joinery)
          teardrop_cube([ z, offsets[i] - offsets[i-1], y ],
                        target_base_width = target_base_width,
                        hole_width = hole_width,
                        penetration = penetration,
                        lip_thickness = lip_thickness,
                        positive_mode = positive_mode,
                        bordered_offset = bordered_offset,
                        tolerance = tolerance);
        else
          cube([ z, offsets[i] - offsets[i-1], y ]);
  }
}

module slicing(
    offsets, i, y, z,
    target_base_width = 7,
    hole_width = 3.5,
    penetration = 5,
    lip_thickness = 2,
    bordered_offset = 0,
    tolerance = 0.01)
{
  if (i == 0)
  {
    slicing_one_bounding_box(
        offsets, i, y, z,
        target_base_width = target_base_width,
        hole_width = hole_width,
        penetration = penetration,
        lip_thickness = lip_thickness,
        positive_mode = true,
        bordered_offset = bordered_offset,
        tolerance = tolerance);
  } else {
    difference()
    {
      slicing_one_bounding_box(
                    offsets, i, y, z,
                    target_base_width = target_base_width,
                    hole_width = hole_width,
                    penetration = penetration,
                    lip_thickness = lip_thickness,
                    positive_mode = true,
                    bordered_offset = bordered_offset,
                    tolerance = tolerance);
      slicing_one_bounding_box(
                    offsets, i-1, y, z,
                    target_base_width = target_base_width,
                    hole_width = hole_width,
                    penetration = penetration,
                    lip_thickness = lip_thickness,
                    positive_mode = false,
                    bordered_offset = bordered_offset,
                    tolerance = tolerance);
    }
  }
}
