/*
 * One piece of the teardrop-shaped joint.
 *
 * Base = flat area where the joint connects to the main material.
 * Hole = the most sticking, cylindrical part of the joint.
 * Neck = the narrowest point of the joint.
 * Neck's offset is the distance from the
 * base to the narrowest point of the joint.
 * Neck's width is proportional to the width of the hole.
 *
 * base_width: width of the area that joins the joint and the material (x axis)
 * hole_width: width of the teardrop cylinder (x axis)
 * penetration: how deeply the joint sticks out (y axis)
 * height: height of the entire joint (z axis)
 *
 * Bounding box of the joint is [max(base_width,hole_width),penetration,height].
 */
module teardrop_single(base_width, hole_width, penetration, height)
{
  neck_offset = (penetration*penetration
                             + base_width*base_width/4)
                             / penetration - hole_width;
  neck_width = (hole_width * base_width)
             / (hole_width + neck_offset);
  cylinder_facets = $preview ? 8 : 36;

  difference()
  {
    union()
    {
      translate([0, penetration-hole_width/2, 0])
        cylinder(d=hole_width,h=height,$fn=cylinder_facets);
      translate([-neck_width/2, 0, 0])
        cube([neck_width, penetration-hole_width/2, height]);
      translate([-base_width/2, 0, 0])
        cube([base_width, neck_offset/2, height]);
    }

    translate([base_width/2,neck_offset/2,-1])
      cylinder(d=neck_offset,h=height+2,$fn=cylinder_facets);
    translate([-base_width/2,neck_offset/2,-1])
      cylinder(d=neck_offset,h=height+2,$fn=cylinder_facets);
  }
}

/*
 * Space the teardrop-shaped joint across a "line".
 *
 * overall_width: width of the entire "line" (x axis)
 * teardrop_target_width: desired width of one of the joints (x axis)
 * hole_width: width of the teardrop cylinder (x axis)
 * penetration: how deeply the joint sticks out (y axis)
 * height: height of the entire joint (z axis)
 */
module teardrop_line(overall_width, teardrop_target_width,
                     hole_width, penetration, height)
{
  count = round(overall_width/teardrop_target_width);
  teardrop_base_width = overall_width/count;

  intersection()
  {
    translate([0, 0, -0.1])
      cube([overall_width, 0.1 + penetration, 0.2 + height]);

    for (i=[1:count])
      translate([(i-0.5) * teardrop_base_width, 0, 0])
      {
        teardrop_single(teardrop_base_width,
            hole_width, penetration, height);
      }
  }
}

/*
 * Space the teardrop-shaped joint across a "line".
 *
 * On outside faces, the teardrop is "halved".
 * Hence we distinguish "inside" and "outside" teardrops.
 *
 * overall_width: width of the entire "line" (x axis)
 * teardrop_target_width: desired width of one of the joints (x axis)
 * inside_hole_width: width of the internal teardrop cylinder (x axis)
 * (Note: width of the external teardrop cylinder is adjusted accordingly.)
 * inside_penetration: penetration of the inside teardrops (y axis)
 * outside_penetration: penetration of the outside teardrops (y axis)
 * height: height of the entire joint (z axis)
 */
module teardrop_line_bordered(overall_width,
                              teardrop_target_width,
                              inside_hole_width,
                              inside_penetration,
                              outside_penetration,
                              height)
{
  max_penetration = max(inside_penetration,outside_penetration);

  // Adjust hole width so that all necks have equal width
  outside_hole_width = 2 * inside_hole_width
                     * outside_penetration
                     / inside_penetration;

  // Each outside "half" teardrop counts as 1/2
  count = round(overall_width/teardrop_target_width);
  teardrop_base_width = overall_width/count;

  intersection()
  {
    translate([0, 0, -0.1])
      cube([overall_width, 0.1 + max_penetration, 0.2 + height]);

    for (i=[0:count])
      translate([i * teardrop_base_width, 0, 0])
      {
        penetration = (i == 0 || i == count)
                    ? outside_penetration
                    : inside_penetration;

        hole_width = (i == 0 || i == count)
                   ? outside_hole_width
                   : inside_hole_width;

        teardrop_single(teardrop_base_width - 0.001,
            hole_width, penetration, height);
        // The "- 0.001" hotfix avoids "UI-WARNING: Object
        // may not be a valid 2-manifold and may need repair!"
      }
  }
}

slza_cube_default = [6, 6];
slza_cube_default_dira = 2.5;
slza_cube_default_skryv = 1;
slza_cube_default_ofset = 1;
slza_cube_default_plat = 1;
slza_epsilon = 0.02;

/*
 * Cube with a teardrop joint at the
 *
 */
module teardrop_cube(dims,
  target_base_width = 6, hole_width = 2.5, penetration = 5, lip_thickness = 2,
  positive_mode = true, bordered_mode = true, bordered_offset = 0.5,
  neck_hidden = 0, tolerance = 0.02)
{
  // Positive piece makes the hole smaller, negative mode makes it bigger.
  adjusted_hole = hole_width + tolerance * (positive_mode ? -1 : +1);

  // If base plate was removed from the negative piece,
  // it couldn't have been printed upside down.
  // Hence, both tolerances are removed from the positive piece.
  base_plate_tolerance = tolerance * 2;
  outside_penetration = penetration + neck_hidden
                      + base_plate_tolerance
                      + tolerance * (positive_mode ? -1 : +1);

  // Lip should not be a friction area, hence the tolerance is 5x bigger.
  adjusted_lip_height = lip_thickness == 0 ? 0
                      : (lip_thickness + tolerance * (positive_mode ? -5 : +5));
  adjusted_lip_penetration = penetration + base_plate_tolerance
                           + tolerance * (positive_mode ? -1 : +1);
  adjusted_thickness = dims.z + tolerance * (positive_mode ? 0 : +5);

  // main cube
  translate([0, base_plate_tolerance, 0])
    cube([dims.x, dims.y-base_plate_tolerance, adjusted_thickness]);

  // lip
  translate([0, tolerance * (positive_mode ? +1 : -1) - penetration, 0])
    cube([dims.x, adjusted_lip_penetration, adjusted_lip_height]);

  // joint
  translate([0, base_plate_tolerance + neck_hidden, 0])
    mirror([0, 1, 0])
      if (bordered_mode)
        teardrop_line_bordered(dims.x, target_base_width, adjusted_hole,
                               outside_penetration - bordered_offset,
                               outside_penetration, adjusted_thickness);
      else
        teardrop_line(dims.x, target_base_width, adjusted_hole,
                      outside_penetration, adjusted_thickness);
}

module teardrop_cube_example()
{
  module example1(
    dims = [21, 8, 7],
    penetration = 5,
    tolerance = 0.02,
    print_mode = false)
  {
    module positive_piece(positive_mode = true)
    {
      teardrop_cube(dims,
          positive_mode = positive_mode,
          penetration = penetration,
          tolerance = tolerance);
    }

    module negative_piece()
    difference()
    {
      translate([0, -dims.y-penetration, 0])
        cube(dims + [0, penetration, 0]);
      positive_piece(false);
    }

    // a) show the positive piece
    translate([0, penetration + 1, 0])
      difference()
      {
        positive_piece();
        translate([0, 1, dims.z-1])
          linear_extrude(2)
            text(str("+", tolerance), size = 5, font="Arial Black");
      }

    // b) show the negative piece
    translate([0, print_mode ? 0 : -penetration-1, print_mode ? dims.z : 0])
      mirror([0, 0, print_mode ? 1 : 0])
        difference()
        {
          negative_piece();
          translate([0, 1 - dims.y - penetration, dims.z-1])
            linear_extrude(2)
              text(str("−", tolerance), size = 5, font="Arial Black");
        }

    // c) show the difference
    if (!print_mode)
      difference()
      {
        translate([0, -10, 0]) cube([21, 15, 7]);
        positive_piece();
        negative_piece();
      }
  } // end of teardrop_cube_example

  module example2(
    dims = [21, 9, 7],
    penetration = 5,
    tolerance = 0.02,
    print_mode = false)
  {
    module positive_piece(positive_mode = true)
    {
      teardrop_cube(dims, lip_thickness = 0,
          positive_mode = positive_mode,
          penetration = penetration,
          tolerance = tolerance);
    }

    module negative_piece()
    difference()
    {
      //translate([0, -dims.y-penetration, 0])
      rotate([90, 0, 0])
        cube(dims + [0, penetration, 0]);
      positive_piece(false);
    }

    // a) show the positive piece
    translate([0, penetration + 1, 0])
      difference()
      {
        positive_piece();
        translate([0, 3, dims.z-1])
          linear_extrude(2)
            text(str("+", tolerance), size = 5, font="Arial Black");
      }

    // b) show the negative piece
    rotate([print_mode ? 90 : 0, 0, 0])
      translate(print_mode ? [0, dims.z, 0] : [0, 0, dims.z + 1])
        difference()
        {
          negative_piece();
          rotate([-90, 0, 0])
          translate([0, 1 - dims.y - penetration, -1])
            linear_extrude(2)
              text(str("−", tolerance), size = 5, font="Arial Black");
        }

    // c) show the difference
    if (!print_mode)
      difference()
      {
        translate([0, -penetration, 0])
          cube([dims.x, penetration, dims.z + tolerance * 5]);
        positive_piece();
        negative_piece();
      }
  } // end of teardrop_cube_example

  scale([2, 2, 2])
  {
    translate([ 0, 0, 0]) example1(tolerance = 0.01, print_mode = true);
    translate([22, 0, 0]) example1(tolerance = 0.02, print_mode = true);
    translate([44, 0, 0]) example1(tolerance = 0.05, print_mode = true);

    translate([ 0, 30, 0]) example2(tolerance = 0.01, print_mode = true);
    translate([22, 30, 0]) example2(tolerance = 0.02, print_mode = true);
    translate([44, 30, 0]) example2(tolerance = 0.05, print_mode = true);
  }
}

teardrop_cube_example();

module slza_cube_z_minus(dims, slza = slza_cube_default,
                         dira = slza_cube_default_dira,
                         skryv = slza_cube_default_skryv,
                         ofset = slza_cube_default_ofset,
                         plat = slza_cube_default_plat)
{
  translate([0, 0, dims.z])
    mirror([0, 0, 1])
      slza_cube_z_plus(dims, slza, dira, skryv, ofset, plat);
}

module slza_cube_y_plus(dims, slza = slza_cube_default,
                         dira = slza_cube_default_dira,
                         skryv = slza_cube_default_skryv,
                         ofset = slza_cube_default_ofset,
                         plat = slza_cube_default_plat)
{
  translate([dims.x, 0, 0])
    rotate([+90, 0, 180])
      slza_cube_z_plus([dims.x, dims.z, dims.y],
                       slza, dira, skryv, ofset, plat);
}

module slza_cube_y_minus(dims, slza = slza_cube_default,
                         dira = slza_cube_default_dira,
                         skryv = slza_cube_default_skryv,
                         ofset = slza_cube_default_ofset,
                         plat = slza_cube_default_plat)
{
  translate([0, dims.y, 0])
    rotate([+90, 0, 0])
      slza_cube_z_plus([dims.x, dims.z, dims.y],
                       slza, dira, skryv, ofset, plat);
}

module slza_cube_x_plus(dims, slza = slza_cube_default,
                         dira = slza_cube_default_dira,
                         skryv = slza_cube_default_skryv,
                         ofset = slza_cube_default_ofset,
                         plat = slza_cube_default_plat)
{
  translate([0, 0, dims.z])
    rotate([0, +90, 0])
      slza_cube_z_plus([dims.z, dims.y, dims.x],
                       slza, dira, skryv, ofset, plat);
}

module slza_cube_x_minus(dims, slza = slza_cube_default,
                         dira = slza_cube_default_dira,
                         skryv = slza_cube_default_skryv,
                         ofset = slza_cube_default_ofset,
                         plat = slza_cube_default_plat)
{
  translate([dims.x, 0, 0])
    rotate([0, -90, 0])
      slza_cube_z_plus([dims.z, dims.y, dims.x],
                       slza, dira, skryv, ofset, plat);
}

module slza_odeber(kompenzace = [0, 0, 0])
{
  assert($children == 2);
  difference()
  {
    children(0);

    translate(kompenzace)
      minkowski()
      {
        //translate([0, 0, -slza_epsilon/2])
          // cylinder(r=slza_epsilon/2,h=slza_epsilon,$fn=4);

        translate([-slza_epsilon/2, -slza_epsilon/2, -slza_epsilon/2])
          cube([slza_epsilon, slza_epsilon, slza_epsilon]);
        children(1);
      }
  }
}
